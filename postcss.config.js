// Assuming 'tailwindcss' and 'autoprefixer' support ES module imports
import tailwindcss from 'tailwindcss';
import autoprefixer from 'autoprefixer';

export default {
  plugins: [
    tailwindcss,
    autoprefixer,
    // Add other plugins here
  ],
};

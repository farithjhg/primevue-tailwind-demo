import { createApp } from 'vue'

import '/node_modules/primeflex/primeflex.css'
import 'primeflex/themes/primeone-light.css'
import "primeicons/primeicons.css";


import 'aos/dist/aos.css';
import './css/tailwind.css'
import './css/style.css'

//PrimeVue CSS configuration
import PrimeVue from 'primevue/config'

import Lara from './presets/lara';

//PrimeVue components
import InputText from 'primevue/inputtext'; // Import InputText
import Button from 'primevue/button'; // Import Button
import Calendar from 'primevue/calendar';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';

import router from './router'
import App from './App.vue'

const app = createApp(App)

app.use(router)
app.use(PrimeVue, {
    unstyled: true,                    //apply preset
    pt: Lara
});

// Register PrimeVue components globally
app.component('InputText', InputText);
app.component('Button', Button);
app.component('Calendar', Calendar);
app.component('DataTable', DataTable);
app.component('Column', Column);

app.mount('#app')